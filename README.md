
# Faster Printing Press

> Register custom template for CPT with OOP.
This package provides you object oriented API to declare template into your CPT, and block pattern easier.


## Authors

- [@benj_grolleau](https://twitter.com/benj_grolleau)
Thanks to [Florian Truchot](https://twitter.com/floriantruchot), [Marie Comet](https://twitter.com/CometMarie), [Dareth Nhang](https://twitter.com/_dsigned) and [Alexandre Buffet](https://twitter.com/alexandrebuffet) for sharing their thoughts during development.

## Installation

Install with composer.
```bash
  composer require benjgrolleau/guttemplates
```

Note : you may use Composer 2 to install FPP.

```bash
composer self-update --2
```
If you wan to rollback to Composer 1, use :
```bash
composer self-update --1
```

## Documentation

### Declare block pattern
Use the function `declare_block_pattern`.

```php
use Gut\Templates\Blocks\Column;
use Gut\Templates\Blocks\Columns;
use Gut\Templates\Blocks\Paragraph;
use Gut\Templates\Blocks\ImageBlock;

register_block_pattern(
    'fpp/custom-pattern',
    array(
        'title'       => __( 'Custom pattern', 'faster-printing-press' ),
        'description' => __( 'Example block.', 'faster-printing-press' ),
        'categories'  => array( 'content' ),
        'content'     => declare_block_pattern([
            Columns::create()
            ->inner([
                Column::create(),
                ->inner([
                    ImageBlock::create()
                ])
                Column::create()
                ->inner([
                    Paragraph::create()
                    ->content( 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, qui?' ),
                ]),
            ]),
        ]),
    )
);

```


### Declare template for post type
Use the function `declare_post_type_template`.

```php
use Gut\Templates\Blocks\Column;
use Gut\Templates\Blocks\Columns;
use Gut\Templates\Blocks\Paragraph;
use Gut\Templates\Blocks\ImageBlock;
use Gut\Templates\Blocks\Heading;
use Gut\Templates\Blocks\Quote;

register_post_type( 'custom-post-type',
    array(
        ...
        'template' => declare_post_type_template([
            Columns::create()
            ->inner([
                Column::create()
                ->inner([
                    Paragraph::create()
                    ->content( 'Here you can write some text'),
                    ImageBlock::create(),
                    Paragraph::create()
                    ->content( 'Another paragraph ?' )
                ]),
                Column::create()
                ->inner([
                    Quote::create()
                    ->content( 'Knowledge is power.' )
                    ->author( 'Francis Bacon' ),
                    Heading::create()
                    ->level(1)
                    ->color( 'primary' )
                    ->placeholder( 'Your title goes here.' )
                ])
            ])
        ])
    )
);

```

### Use with your own blocks
You can easily extends with your own blocks.
```php
namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Color;

class CustomBlock extends Block {

	use Color;
	use ClassName;

	protected $name = 'custom-block';
	protected $tag  = 'div';
}

```

If you want to rewrite the render function, use the method `renderPattern()` for pattern render, and `renderTemplate()` for post type template.

## Feedback

If you have any feedback, please reach out to me at hello@benjamin-grolleau.fr.
This is my first open-source project, so don't hesitate to give me your feedback.
Feel free to say hi on [Twitter](https://twitter.com/benj_grolleau) ! 👋
## Roadmap

1. Add all content blocks.
2. Add a CLI to generate FSE templates.

## References
They don't know, but they help me to build this project.

 - [WordPlate](https://github.com/wordplate/wordplate) team, with the [Extended ACF library](https://github.com/wordplate/extended-acf) that inspired me a lot.
 - [readme.so](https://readme.so/), who helped me write what you are reading now.

### Buy me a coffee ☕️
If you like it, you can [support my work here](https://www.buymeacoffee.com/benjgrolleau) !
Thanks.