<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks\Attributes;

use InvalidArgumentException;

trait Level {
	public function level( int $level ) {
		if ( $level > 6 || $level < 1 ) {
			throw new InvalidArgumentException( 'The level must be a value between 1 and 6.' );
		}
		$this->settings['level'] = $level;
		$this->tag = 'h' . $level;
		return $this;
	}
}
