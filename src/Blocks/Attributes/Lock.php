<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks\Attributes;

trait Lock {
	public function lockMove() {
		$this->settings['lock']['move'] = true;
		return $this;
	}

	public function lockRemove() {
		$this->settings['lock']['remove'] = true;
		return $this;
	}

	public function lockAll() {
		$this->lockRemove();
		$this->lockMove();
		return $this;
	}
}
