<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks\Attributes;

use InvalidArgumentException;

trait Target {
	public function newTab() {
		$this->linkSettings['target'] = '_blank';
		$this->settings['linkTarget'] = '_blank';
		return $this;
	}
}
