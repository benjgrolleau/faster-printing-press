<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks\Attributes;

use InvalidArgumentException;

trait Align {
	public function align( $align ) {
		if ( ! \in_array( $align, [ 'left', 'center', 'right', 'wide', 'full' ] ) ) {
			throw new InvalidArgumentException( 'The align value is not supported.' );
		}
		$this->settings['align'] = $align;
		return $this;
	}
}
