<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks\Attributes;

use InvalidArgumentException;

trait TextTransform {
	public function textTransform( $style ) {
		if ( ! \in_array( $style, [ 'uppercase', 'lowercase', 'capitalize' ] ) ) {
			throw new InvalidArgumentException( 'The text transform value is not supported.' );
		}
		$this->settings['typography']['textTransform '] = $style;
		return $this;
	}
}
