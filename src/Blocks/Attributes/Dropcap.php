<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks\Attributes;

trait Dropcap {
	public function dropcap() {
		$this->settings['dropCap'] = true;
		return $this;
	}
}
