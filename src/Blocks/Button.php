<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Width;
use Gut\Templates\Blocks\Attributes\ClassName;
use Gut\Templates\Blocks\Attributes\Url;
use Gut\Templates\Blocks\Attributes\Title;
use Gut\Templates\Blocks\Attributes\Content;
use Gut\Templates\Blocks\Attributes\Target;
use Gut\Templates\Blocks\Attributes\Rel;
use Gut\Templates\Blocks\Attributes\Placeholder;
use Gut\Templates\Blocks\Attributes\Background;
use Gut\Templates\Blocks\Attributes\Color;

class Button extends Block {

	use Content, Target, Rel, Placeholder, Background, Color, Title, Url, ClassName, Width;

	protected $name         = 'button';
	protected $tag          = 'div';
	protected $linkSettings = array();

	public function renderPattern() : string {

		$linksAttributes = '';

		isset( $this->settings['className'] ) ? $classes = ' class="' . $this->settings['className'] . '"' : $classes = '';

		$json_params = ! empty( $this->settings ) ? json_encode( $this->settings ) . ' ' : '';

		if ( isset( $this->linkSettings ) ) :
			foreach ( $this->linkSettings as $attribute => $value ) :
				$linksAttributes .= $attribute . '="' . $value . '" ';
			endforeach;
		endif;

		return '<!-- wp:' . $this->name . ' ' . $json_params . '--><' . $this->tag . $classes . '><a class="wp-block-button__link" ' . $linksAttributes . ' >' . $this->content . '</a></' . $this->tag . '><!-- /wp:' . $this->name . ' -->';
	}
}
