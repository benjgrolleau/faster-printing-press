<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Inner;
use Gut\Templates\Blocks\Attributes\Height;

class Spacer extends Block {

	use Inner;
	use Height;

	protected $name = 'spacer';
	protected $tag  = 'div';

	public function renderPattern() : string {
		return '<!-- wp:' . $this->name . ' --><div class="' . $this->settings['className'] . '" aria-hidden="true" style="height:' . $this->settings['height'] . 'px"></div><!-- /wp:' . $this->name . ' -->';
	}
}
