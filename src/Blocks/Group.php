<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Inner;
use Gut\Templates\Blocks\Attributes\Width;
use Gut\Templates\Blocks\Attributes\ClassName;
use Gut\Templates\Blocks\Attributes\Align;
use Gut\Templates\Blocks\Attributes\Color;
use Gut\Templates\Blocks\Attributes\Background;
use Gut\Templates\Blocks\Attributes\FontSize;

class Group extends Block {

	use Inner, Align, Width, ClassName, Color, Background, FontSize;

	protected $name = 'group';
	protected $tag  = 'div';
}