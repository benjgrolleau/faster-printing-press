<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Inner;
use Gut\Templates\Blocks\Attributes\Width;
use Gut\Templates\Blocks\Attributes\ClassName;
use Gut\Templates\Blocks\Attributes\Anchor;
use Gut\Templates\Blocks\Attributes\Align;
use Gut\Templates\Blocks\Attributes\Src;
use Gut\Templates\Blocks\Attributes\Caption;

class Audio extends Block {

	use Inner, Anchor, Align, Width, ClassName, Src, Caption;

	public function autoplay() {
		$this->settings['autoplay'] = true;
		return $this;
	}

	public function loop() {
		$this->settings['loop'] = true;
		return $this;
	}

	/**
	 * Render the block for block pattern.
	 */
	public function renderPattern() : string {
		if ( isset( $this->inner ) ) {
			$inner_string = '';
			foreach ( $this->inner as $block ) {
				$inner_string .= $block->renderPattern();
			}
			$this->content .= $inner_string;
		}

		if ( isset( $this->settings['caption'] ) ) {
			$this->content .= '<figcaption>' . $this->settings['caption'] . '</figcaption>';
			unset( $this->settings['caption'] );
		}

		if ( isset( $this->settings['src'] ) ) {
			$this->content .= '<audio controls';

			if ( isset( $this->settings['autoplay'] ) ) {
				$this->content .= ' autoplay';
				unset( $this->settings['autoplay'] );
			}

			if ( isset( $this->settings['loop'] ) ) {
				$this->content .= ' loop';
				unset( $this->settings['loop'] );
			}

			if ( isset( $this->settings['src'] ) ) {
				$this->content .= " src='" . $this->settings['src'] . "'";
				unset( $this->settings['src'] );
			}

			$this->content .= '></audio>';

			if ( isset( $this->settings['caption'] ) ) {
				$this->content .= '<figcaption>' . $this->settings['caption'] . '</figcaption>';
				unset( $this->settings['caption'] );
			}
		}

		isset( $this->settings['className'] ) ? $classes = ' class="' . $this->settings['className'] . '"' : $classes = '';
		$json_params                                     = ! empty( $this->settings ) ? json_encode( $this->settings ) . ' ' : '';
		return '<!-- wp:' . $this->name . ' ' . $json_params . '--><' . $this->tag . $classes . '>' . $this->content . '</' . $this->tag . '><!-- /wp:' . $this->name . ' -->';
	}

	protected $name = 'audio';
	protected $tag  = 'figure';
}
