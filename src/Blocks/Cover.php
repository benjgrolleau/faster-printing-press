<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Inner;
use Gut\Templates\Blocks\Attributes\Width;
use Gut\Templates\Blocks\Attributes\ClassName;
use Gut\Templates\Blocks\Attributes\Align;
use Gut\Templates\Blocks\Attributes\Color;
use Gut\Templates\Blocks\Attributes\Background;
use Gut\Templates\Blocks\Attributes\FontSize;

use InvalidArgumentException;

class Cover extends Block {

	use Inner, Align, Width, ClassName, Color, Background, FontSize;

	public function useFeaturedImage() {
		$this->settings['useFeaturedImage'] = true;
		return $this;
	}

	public function src( string $src ) {
		$this->settings['url'] = $src;
		return $this;
	}

	public function parallax() {
		$this->settings['hasParallax'] = true;
		$this->settings['className']  .= ' has-parallax';
		return $this;
	}

	public function hasDim( int $ratio = 100 ) {
		if ( $ratio % 10 !== 0 ) {
			throw new InvalidArgumentException( 'Background dim must be multiple of 10.' );
		}
		$this->settings['dimRatio']   = $ratio;
		$this->settings['className'] .= ' has-background-dim-' . $ratio;

		$this->content .= '<span aria-hidden="true" class="wp-block-cover__background has-background-dim"></span>';
		return $this;
	}

	public function focalPoint( array $focal = array(
		'x' => .5,
		'y' => .5,
	) ) {
		// TODO : On the block-cover__image-background, add 'style="object-position:14% 13%" data-object-fit="cover" data-object-position="14% 13%"'
		$this->settings['focalPoint'] = $focal;
		return $this;
	}

	public function minHeight( int $height, string $unit = 'px' ) {
		if ( ! \in_array( $unit, array( 'px', 'rem', 'em', 'vw', '%', 'vh' ) ) ) :
			throw new InvalidArgumentException( 'Unit variable must be px, rem, em, vw, % or vh.' );
		endif;
		$this->settings['minHeight'] = $height . $unit;
		return $this;
	}

	public function repeatBackground() {
		$this->settings['isRepeated'] = true;
		return $this;
	}

	protected $name = 'cover';
	protected $tag  = 'div';
}
