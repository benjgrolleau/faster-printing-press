<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Placeholder;
use Gut\Templates\Blocks\Attributes\Align;
use Gut\Templates\Blocks\Attributes\Anchor;
use Gut\Templates\Blocks\Attributes\Background;
use Gut\Templates\Blocks\Attributes\ClassName;
use Gut\Templates\Blocks\Attributes\Color;
use Gut\Templates\Blocks\Attributes\Dropcap;
use Gut\Templates\Blocks\Attributes\FontSize;
use Gut\Templates\Blocks\Attributes\LineHeight;
use Gut\Templates\Blocks\Attributes\Content;

class Quote extends Block {
	use Align;
	use Anchor;
	use Background;
	use ClassName;
	use Color;
	use Content;
	use Dropcap;
	use FontSize;
	use LineHeight;
	use Placeholder;

	protected $name   = 'quote';
	protected $tag    = 'blockquote';
	protected $author = '';

	public function author( $author ) {
		$this->author = $author;
		return $this;
	}

	public function renderPattern() : string {
		return '<!-- wp:' . $this->name . ' ' . json_encode( $this->settings ) . ' --><' . $this->tag . ' class="' . $this->settings['className'] . '"><p>' . $this->content . '</p><cite>' . $this->author . '</cite></' . $this->tag . '><!-- /wp:' . $this->name . ' -->';
	}
}
