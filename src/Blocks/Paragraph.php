<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Placeholder;
use Gut\Templates\Blocks\Attributes\Align;
use Gut\Templates\Blocks\Attributes\Anchor;
use Gut\Templates\Blocks\Attributes\Background;
use Gut\Templates\Blocks\Attributes\ClassName;
use Gut\Templates\Blocks\Attributes\Color;
use Gut\Templates\Blocks\Attributes\Dropcap;
use Gut\Templates\Blocks\Attributes\FontSize;
use Gut\Templates\Blocks\Attributes\LineHeight;
use Gut\Templates\Blocks\Attributes\Content;

class Paragraph extends Block {
	use Align;
	use Anchor;
	use Background;
	use ClassName;
	use Color;
	use Content;
	use Dropcap;
	use FontSize;
	use LineHeight;
	use Placeholder;

	protected $name = 'paragraph';
	protected $tag  = 'p';
}
