<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Align;
use Gut\Templates\Blocks\Attributes\Anchor;
use Gut\Templates\Blocks\Attributes\ClassName;


class ImageBlock extends Block {
	use Align;
	use Anchor;
	use ClassName;

	protected $name   = 'image';
	protected $tag    = 'figure';

	public function renderPattern() : string {
		return '<!-- wp:' . $this->name . ' ' . json_encode( $this->settings ) . ' --><' . $this->tag . ' class="' . $this->settings['className'] . '"><img alt=""></img></' . $this->tag . '><!-- /wp:' . $this->name . ' -->';
	}
}
