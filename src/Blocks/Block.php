<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

abstract class Block {

	protected array $settings;
	protected $type    = 'core/';
	protected $tag     = 'div';
	protected $content = '';
	protected $name;

	public function __construct() {
		$this->type                 .= $this->name;
		$this->settings['className'] = 'wp-block-' . $this->name;
	}

	public static function create() : object {
		return new static();
	}

	/**
	 * Render the block for post type template.
	 */
	public function renderTemplate() : array {
		$inner_render = [];
		if ( isset( $this->inner ) ) {
			foreach ( $this->inner as $block ) {
				$inner_render[] = $block->renderTemplate();
			}
		}
		return [ $this->type, $this->settings, ! empty( $inner_render ) ? $inner_render : [] ];
	}

	/**
	 * Render the block for block pattern.
	 */
	public function renderPattern() : string {
		if ( isset( $this->inner ) ) {
			$inner_string = '';
			foreach ( $this->inner as $block ) {
				$inner_string .= $block->renderPattern();
			}
			$this->content .= $inner_string;
		}
		isset( $this->settings['className'] ) ? $classes = ' class="' . $this->settings['className'] . '"' : $classes = '';
		$json_params                                     = ! empty( $this->settings ) ? json_encode( $this->settings ) . ' ' : '';
		return '<!-- wp:' . $this->name . ' ' . $json_params . '--><' . $this->tag . $classes . '>' . $this->content . '</' . $this->tag . '><!-- /wp:' . $this->name . ' -->';
	}
}
