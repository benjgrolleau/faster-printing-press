<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

namespace Gut\Templates\Blocks;

use Gut\Templates\Blocks\Block;
use Gut\Templates\Blocks\Attributes\Content;

class Shortcode extends Block {
	use Content;

	protected $name = 'shortcode';
	protected $tag  = 'p';

	public function renderPattern() : string {
		return '<!-- wp:' . $this->name . '-->' . $this->content . '<!-- /wp:' . $this->name . ' -->';
	}

	/**
	 * Render the block for post type template.
	 */
	public function renderTemplate() : array {
		$inner_render = [];
		return [ $this->type, $this->settings, ! empty( $inner_render ) ? $inner_render : [] ];
	}
}
