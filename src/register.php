<?php
/**
 * Copyright (c) Benjamin Grolleau.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://gitlab.com/benjgrolleau/faster-printing-press/
 */

if ( ! function_exists( 'declare_post_type_template' ) ) {

	/**
	 * Render an array of objects, ready to be used in post type template declaration.
	 *
	 * @param array $blocks The blocks to render.
	 * @return array
	 */
	function declare_post_type_template( array $blocks ) : array {
		$template = [];
		foreach ( $blocks as $block ) {
			$template[] = $block->renderTemplate();
		}
		return $template;
	}
}

if ( ! function_exists( 'declare_block_pattern' ) ) :

	/**
	 * Render the string for Gutenberg Block Pattern.
	 *
	 * @see register_block_pattern();
	 *
	 * @param array $blocks The block to render.
	 * @return string
	 */
	function declare_block_pattern( array $blocks ) {
		$pattern = '';
		foreach ( $blocks as $block ) {
			$pattern .= $block->renderPattern();
		}
		return $pattern;
	}
endif;
